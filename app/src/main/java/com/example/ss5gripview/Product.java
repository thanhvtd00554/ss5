package com.example.ss5gripview;

public class Product {
    private String Name;
    private String Price;
    private String Des;
    private int Image;

    public Product(String name, String price, String des, int image) {
        Name = name;
        Price = price;
        Des = des;
        Image = image;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getDes() {
        return Des;
    }

    public void setDes(String des) {
        Des = des;
    }

    public int getImage() {
        return Image;
    }

    public void setImage(int image) {
        Image = image;
    }
}
